# Find Enrollment packages which are missing meetingDaysList

This script will identify any enrollment packages in the Course Search & Enroll
app's search index which do not have their `meetingDaysList` field populated.

This is likely due to an upstream issue, and manifests itself as generated schedules
not displaying all courses. See [ROENROLL-1306](https://jira.doit.wisc.edu/jira/browse/ROENROLL-1306)


## Requirements

* Bash v2 or later
* [curl](https://curl.haxx.se/) for HTTP
* [jq](https://stedolan.github.io/jq/) for parsing JSON
* An IP address which is whitelisted to access the Elasticsearch index


## Running

	# TIER is one of "test", "prod", or "red"
	./missingMeetingDays TIER
	
Each time the script runs, it will move the output file, `package_ids.txt`, to a
version appended with `.old` and truncate the data file. It then queries all available
terms for any enrollment packages lacking `meetingDaysList` and stores the list of
package IDs in the output file.
