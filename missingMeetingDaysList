#!/bin/bash
# Query elasticsearch for any enrollment packages missing meetingDaysList

### The Elasticsearch URL by deployment tier
declare -A url
# prod AWS
url[prod]="https://search-prod-enrollment-tools-kpxqmr27ymqjjsbyp4v2fxomka.us-east-2.es.amazonaws.com"
# test AWS
url[test]="https://search-enrollment-tools-test-pogqwe24hix7mzoyozhrvnj4f4.us-east-2.es.amazonaws.com"
# test RED (unused by the app, but occasionally handy for testing)
url[red]="http://red.doit.wisc.edu:9200"

if [[ $# -lt 1 ]]; then
	me=$(basename "$0")
	printf "$me: missing tier argument\n\n"
	echo "Usage: $me TIER [TIER]..."
	echo "  where TIER is one or more of: red, test, prod"
	exit 1
fi


# See how many affected enrollment packages exist in the provided term
function affected_package_count() {
	local term=$1
	local elasticsearch=$2
	package_count=`curl --silent -X POST "${elasticsearch}/courses${term}/enrollmentPackage/_search" \
			--data @- << QUERY | jq '.hits.total'
{
	"size": "0",
	"query": {
		"filtered": {
			"filter": {
				"bool": {
					"must": [
						{ "exists": { "field": "classMeetings.meetingDays" } },
						{ "missing": { "field": "classMeetings.meetingDaysList" } }
					]
				}
			}
		}
	}
}
QUERY`
	echo $package_count
}

# Query Elasticsearch for the affected package IDs, appending them to ${term}.txt
function get_affected_package_ids() {
	local term=$1
	local elasticsearch=$2
	local count=$(affected_package_count $term $elasticsearch)
	echo "Found $count enrollment packages in $term with missing 'meetingDaysList', printing package IDs to $outfile"
	if (( count > 10000 )); then
		# This can be remedied by using the scroll API
		echo "WARNING: Elasticsearch won't let us fetch beyond 10000 documents, so we're not getting them all." 
	fi
	declare -i from="0" increment="1000"
	while (( from < count && from < 10000 )); do
		echo "Querying $term from $from to $((from + increment))"
		search_results=`curl --silent -X POST -H "Content-Type: application/json" "${elasticsearch}/courses${term}/enrollmentPackage/_search?size=$increment&from=$from" \
			--data @- <<QUERY 
{
	"fields": ["_id"],
	"query": {
		"filtered": {
			"filter": {
				"bool": {
					"must": [
						{ "exists": { "field": "classMeetings.meetingDays" } },
						{ "missing": { "field": "classMeetings.meetingDaysList" } }
					]
				}
			}
		}
	}
}
QUERY`

		# Pipe the Elasticsearch results through jq to select the package ID field 
		# and then through sed to get rid of quotes.
		echo $search_results | jq '.hits.hits[] | ._id' | sed 's/"//g' | sort >> $outfile
		from+=$increment
	done
}



for tier in $@; do
	if [[ -v url[$tier] ]]; then
		elasticsearch=${url[$tier]}
		printf "\n\nQuerying the $tier tier at $elasticsearch\n"
	else
		echo "ERROR: unknown tier '$tier', skipping"
		continue
	fi

	### The output file which will contain a list of all affected enrollment package IDs
	outfile="${tier}_package_ids.txt"


	# Save the data file from the last run and truncate the file for this run
	if [[ -f $outfile ]]; then
		echo "Moving $outfile to ${outfile}.old and truncating original"
		mv --force $outfile ${outfile}.old
		> $outfile
	else
		touch $outfile
	fi

	# Gather the affected package IDs for each term
	course_indices=`curl --silent ${elasticsearch}/_cat/indices/courses*?h=index&s=index`
	for index in $course_indices; do
		# Extract the term code from the coursesXXXX index name
		termCode="${index//[!0-9]/}"
		get_affected_package_ids $termCode $elasticsearch
	done
done
